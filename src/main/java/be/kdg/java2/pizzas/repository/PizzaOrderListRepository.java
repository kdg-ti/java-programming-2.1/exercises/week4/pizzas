package be.kdg.java2.pizzas.repository;

import be.kdg.java2.pizzas.domain.PizzaOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PizzaOrderListRepository extends ListRepository<PizzaOrder>{
    private static final Logger log = LoggerFactory.getLogger(PizzaOrderListRepository.class);

    public PizzaOrderListRepository(){
        log.debug("Creating pizzaorder repository");
    }
}
