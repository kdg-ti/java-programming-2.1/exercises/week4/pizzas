package be.kdg.java2.pizzas.repository;

import be.kdg.java2.pizzas.domain.Ingredient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class IngredientsListRepository extends ListRepository<Ingredient>{
    private static final Logger log = LoggerFactory.getLogger(PizzaOrderListRepository.class);

    public IngredientsListRepository(){
        log.debug("Creating ingredients repository");
    }
}
